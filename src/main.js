import Vue from 'vue'
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import VueApollo from 'vue-apollo';
import './config/buefy';
import './assets/css/main.scss';

import App from './App'

const link = new HttpLink({
  uri: process.env.GRAPHQL_ENDPOINT,
});

const apolloClient = new ApolloClient({
  link,
  cache: new InMemoryCache(),
});

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
})

Vue.use(VueApollo);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  provide: apolloProvider.provide(),
  template: '<App/>'
})
