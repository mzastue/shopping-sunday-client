'use strict'
const detenv = require('dotenv').config();

module.exports = {
  NODE_ENV: '"production"',
  GRAPHQL_ENDPOINT: JSON.stringify(process.env.GRAPHQL_ENDPOINT),
}
